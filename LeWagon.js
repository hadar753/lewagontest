
/* The function return the multiply result of the weight of the max shared habbits between two friends
    Params - 
        friendsNodes - number of friends
        numOfEdges - number of connections between friends
        connections - array of connections (from, to, weight)
    Return - 
        multiply of max shared hobbit result (num)
*/
function maxShared(friendsNodes, numOfEdges, connections) {
    // Will contain the weight of connection between every combination of 2 friends
    var sumConnectionWeight = [];
    var max = 0;

    // Initializing the sumConnectionWeight 2 dimensional array
    for ( var i = 1; i <= friendsNodes; i++ ) {
        sumConnectionWeight[i] = []; 
    }

    for(let con of connections) {
        if(!sumConnectionWeight[con.from][con.to]) // If there is no previous connection between this combination of 2 friends
            sumConnectionWeight[con.from][con.to] = con.weight;
        else 
            sumConnectionWeight[con.from][con.to] *= con.weight;

        if(sumConnectionWeight[con.from][con.to] > max)
            max = sumConnectionWeight[con.from][con.to];
    }
    return max;
}

x = maxShared(4, 5, [{from: 1, to: 2, weight: 2}, {from: 1, to: 2, weight: 3}, {from: 2, to: 3, weight: 1}, {from: 2, to: 3, weight: 3}, {from: 2, to: 4, weight: 3}]);